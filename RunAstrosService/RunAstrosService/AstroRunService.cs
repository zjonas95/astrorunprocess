﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Runtime.InteropServices;
using System.Data.SQLite;
using System.IO;
using System.IO.Compression;
using System.Xml;
using Microsoft.Web.Administration;
using System.Text.RegularExpressions;

namespace RunAstrosService
{
    public partial class AstroRunService : ServiceBase
    {
        EventLog EventLog1;
        int eventId = 1;
        ServiceStatus serviceStatus;
        private SQLiteConnection sqlite;
        private string versionPath = @"C:\QA\Run Astro Continuous\";
        string msbuildPath = @"C:\Program Files (x86)\MSBuild\14.0\Bin\msbuild.exe";
        string vsTestConsolePath = @"C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\CommonExtensions\Microsoft\TestWindow\vstest.console.exe";
        bool debugLogging = false;

        public AstroRunService()
        {
            InitializeComponent();

            EventLog1 = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists("AstroProcessLog"))
               System.Diagnostics.EventLog.CreateEventSource("AstroProcessLog", "AstroRunProcessLog");
            
            EventLog1.Source = "AstroProcessLog";
            EventLog1.Log = "AstroRunProcessLog";
        }

        internal void TestStartupAndStop(string[] args)
        {
            this.OnStart(args);
            Console.ReadLine();
            this.OnStop();
        }

        static void Main(string[] args)
        {
            if(Environment.UserInteractive)
            {
                AstroRunService service1 = new AstroRunService();  
                service1.TestStartupAndStop(args);
            }
        }

        protected override void OnStart(string[] args)
        {
            writeToEventLog("In OnStart", EventLogEntryType.Information);
            //set service state to starting up
            serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            sqlite = new SQLiteConnection(@"Data Source=C:\QA\Zack\TestDataBase\BldNbrMaint.db");

            Timer timer = new Timer();
            timer.Interval = 1000000; //poll every 5 seconds
            timer.Elapsed += new ElapsedEventHandler(this.OnTimer);
            timer.Start();

            // Update the service state to Running.
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            //Copies over Backup Files to the versionPath if it doesn't exist there already
            if (!Directory.Exists(versionPath + "\\Backup Files"))
            {
                var backupDirectory = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()))) + "\\Backup Files";
                Directory.CreateDirectory(versionPath + "\\Backup Files");
                foreach(string filePath in Directory.GetFiles(backupDirectory))
                {
                    File.Copy(filePath, versionPath + "\\Backup Files\\" + Path.GetFileName(filePath));
                }
            }

            //kicks off the first time so we don't have to wait the full time interval for the process to start
            OnTimer(null, null);
        }

        protected override void OnContinue()
        {
            writeToEventLog("In OnContinue.");
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            //Apply appropriate logic to poll for all of the correct branches while limiting the number of "working jobs" we can have running
            writeToEventLog("Monitoring the System");
            string branchToPoll = "master";
            string query = "SELECT MAX(buildDate) as \"Date Built\", branch, buildNumber, jobNbr FROM BuildLog WHERE branch = \"" + branchToPoll + "\"";
            DataTable buildTable = checkForBuilds(query);
            string date = buildTable.Rows[0].ItemArray[0].ToString();
            string fullBranch = buildTable.Rows[0].ItemArray[1].ToString();

            string branch = fullBranch.Replace("QA/","");
            branch.Replace("Production/","");
            //add replacement for the branch variable for any other branch paths that might occur, such as issuefix/, etc/, or others if they come up

            string buildNumber = buildTable.Rows[0].ItemArray[2].ToString();
            string jobNbr = buildTable.Rows[0].ItemArray[3].ToString();

            string message = String.Format("Branch '{0}' was built at {1}. Job Number: {2} buildNumber: {3}", branch, date, buildNumber, jobNbr);
            writeToEventLog(message, EventLogEntryType.SuccessAudit);

            //getExagoInstaller
            setupAppPool(branch); 
            installExago(branch);

            setupSite(branch);
            //refactor the above methods to do a dictionary lookup as opposed to looping through ie. if appPool['name' ] == null { mkpool }
            //ToDo: find out how to use todos

            pullAstros(branch);
            setupWebReportsRefrences(branch);
            buildAstros(branch);
            Process schedulerProcess = setupScheduler(branch);
            
            //Run Astros
            runAstros(branch);
            
            //stop the scheduler
            schedulerProcess.Kill();
            writeToEventLog("Scheduler Process Ended", EventLogEntryType.SuccessAudit);
            
            //Report on results
        }

        private DataTable checkForBuilds(string query)
        {
            SQLiteDataAdapter ad;
            DataTable dt = new DataTable();
            writeToEventLog("Attempting to query database");
            try
            {
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = query;  //set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource
                writeToEventLog("Succesfully queried the database", EventLogEntryType.SuccessAudit);
            }
            catch (SQLiteException ex)
            {
                string message = "Failed to query the database with error: " + ex.Message;
                writeToEventLog(message, EventLogEntryType.Error);
            }

            sqlite.Close();
            return dt;
        }

        private void pullAstros(string version)
        {
            string branch = version;
            if (version != "master")
                branch = "Versions/" + version;

            string astroFolder = versionPath + version + @"\Astro Folder";
            //Create the astro folder if it doesn't already exist
            if (!Directory.Exists(astroFolder))
                Directory.CreateDirectory(astroFolder);

            //Clones the astro solution if it doesn't already exist, move on to pulling the updated solution
            if (!System.IO.File.Exists(astroFolder + @"\Astro.SLN"))
             {
                writeToEventLog("Attempting to clone the repository File Exists is: " + System.IO.File.Exists(astroFolder + "Astro.SLN") + " on " + astroFolder + "Astro.SLN");
                //RunProcess("git.exe", "--git-dir=\"" + versionPath + version + "\\Astro Folder\" clone https://zjonas95@bitbucket.org/exagodev/seleniumbdd.git");
                RunProcess("git.exe", "clone https://zjonas95@bitbucket.org/exagodev/seleniumbdd.git " + "\"" + astroFolder + "\"");
                writeToEventLog("Successfully cloned the repository", EventLogEntryType.SuccessAudit);
            }

            //Pull the astro solution, including a hard reset, checking out the proper branch, and pulling
            
            //Hard reset the repository
            writeToEventLog("Reseting all working changes to the branch: " + branch + " in folder: " + astroFolder + @"\.git");
            RunProcess("git.exe", "-C " + "\"" + astroFolder + "\"" + " reset --hard");
            //Clean the repository - this is necessary in the case of unstaged changes (can happen occasionally with commits to the astro project)
            writeToEventLog("Cleaning branch: " + branch + " in folder: " + astroFolder + @"\.git");
            RunProcess("git.exe", "-C " + "\"" + astroFolder + "\"" + " clean -fd");
            
            //Checkout the correct branch
            writeToEventLog("Checking out the branch: " + branch + " with folder: " + astroFolder + @"\.git");
            RunProcess("git.exe", "-C " + "\"" + astroFolder + "\"" + " checkout " + "\"" + branch + "\"");
            
            //Pull repository
            writeToEventLog("Pulling the repository: " + versionPath + version + "\\Astro Folder\\.git");
            RunProcess("git.exe", "-C " + "\"" + astroFolder + "\"" + " pull");

            writeToEventLog("Repository succesfully updated", EventLogEntryType.SuccessAudit);
        }

        private Process setupScheduler(string version)
        {
            string schedulerPort;
            if (version == "master")
                schedulerPort = "1000";
            else
                schedulerPort = Regex.Replace(version, @"\D", "");

            string schedulerFolder = versionPath + version + "\\Exago Folder\\Scheduler";
            File.Copy(versionPath + "\\Backup Files\\eWebReportsScheduler.xml", schedulerFolder + "\\eWebReportsScheduler.xml", true);

            //Applies the proper AppPath and WebReportsBaseUrl properties to WebReportsApi.xml 
            XmlDocument eWebReportsSchedulerXml = new XmlDocument();
            eWebReportsSchedulerXml.Load(schedulerFolder + "\\eWebReportsScheduler.xml");

            XmlNode port = eWebReportsSchedulerXml.SelectSingleNode("//port");
            port.InnerText = schedulerPort;
            eWebReportsSchedulerXml.Save(schedulerFolder + "\\eWebReportsScheduler.xml");


            writeToEventLog("Starting the scheduler");
            Process schedulerProcess = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.FileName = versionPath + version + "\\Exago Folder\\Scheduler\\eWebReportsScheduler.exe";
            schedulerProcess.StartInfo = startInfo;
            schedulerProcess.Start();
            return schedulerProcess;
        }

        private void runAstros(string version)
        {
            string astroFolder = versionPath + version + @"\Astro Folder\";

            string runsettingsPath = setupRunsettingsFile(astroFolder, version); //setup runsettings file and return path to it
            string [] projectList = { "Extensibility", "Integration", "TestAutomations", "UserInterface", "Utilities", "Workflows"};
            string dllPathsToExecute = "";

            foreach(string project in projectList)
                dllPathsToExecute += "\"" + astroFolder + project + @"\bin\Debug\" + project + ".dll" + "\" ";

            RunProcess(vsTestConsolePath, dllPathsToExecute + " /Settings:\"" + runsettingsPath + "\"");
        }

        private string setupRunsettingsFile(string astroFolder, string version)
        {
            string exagoWebFolder = versionPath + version + "\\Exago Folder\\Web\\";
            string versionDigits = Regex.Replace(version, @"\D", "");
            if (version == "master")
                versionDigits = "1000";
            string committedRunsettingsPath = astroFolder + "Local.Runsettings";
            string runsettingsPath = astroFolder + "RunAstrosContinuousRunSettings" + versionDigits + ".runsettings";


            //Copy Local.Runsettings (our sudo-template runsettings file) over to the new runsettings path and update the values
            //This section of code will need to be updated if any runsettings are added or modified
            File.Copy(committedRunsettingsPath, runsettingsPath, true);

            XmlDocument runsettings = new XmlDocument();
            runsettings.Load(runsettingsPath);

            runsettings.SelectSingleNode("//Parameter[@name = 'browser']").Attributes["value"].Value = "headless";
            runsettings.SelectSingleNode("//Parameter[@name = 'version']").Attributes["value"].Value = version;
            runsettings.SelectSingleNode("//Parameter[@name = 'driverPath']").Attributes["value"].Value = astroFolder + "WebDriver\\";
            runsettings.SelectSingleNode("//Parameter[@name = 'testUrl']").Attributes["value"].Value = "http://localhost/ExagoAstro_" + version + "/";
            runsettings.SelectSingleNode("//Parameter[@name = 'appPath']").Attributes["value"].Value = exagoWebFolder;
            runsettings.SelectSingleNode("//Parameter[@name = 'appUrl']").Attributes["value"].Value = "http://localhost/ExagoAstro_" + version + "/";
            runsettings.SelectSingleNode("//Parameter[@name = 'freshConfigFile']").Attributes["value"].Value = astroFolder + "Configs\\mssql.xml";
            runsettings.SelectSingleNode("//Parameter[@name = 'workingConfigFile']").Attributes["value"].Value = exagoWebFolder + "Config\\mssql.xml";
            runsettings.SelectSingleNode("//Parameter[@name = 'reportsDirectory']").Attributes["value"].Value = astroFolder + "Reports";
            runsettings.SelectSingleNode("//Parameter[@name = 'workingReportsDirectory']").Attributes["value"].Value = versionPath + "Reports";
            runsettings.SelectSingleNode("//Parameter[@name = 'apiPath']").Attributes["value"].Value = "http://localhost/ExagoAstroApi_" + version + "/";
            runsettings.SelectSingleNode("//Parameter[@name = 'schedulerport']").Attributes["value"].Value = versionDigits;

            runsettings.Save(runsettingsPath);
            
            if(!Directory.Exists(versionPath + "Reports"))
            {
                Directory.CreateDirectory(versionPath + "Reports");
            }

            return runsettingsPath;
        }
        private void installExago(string version)
        {
            string exagoFolder = versionPath + version + "\\Exago Folder\\";

            //Delete the exago directory if it already exists
            if (Directory.Exists(exagoFolder))
                Directory.Delete(exagoFolder, true);
            Directory.CreateDirectory(exagoFolder);

            //Gets zipped Exago and unzips into Exago Folder directory
            string[] filePaths = System.IO.Directory.GetFiles(versionPath + version + "\\", "*.zip");
            try
            {
                ZipFile.ExtractToDirectory(filePaths[0], exagoFolder);
            }
            catch (Exception e)
            {
                string message = "Exago unzip failed with error: " + e.Message;
                writeToEventLog(message, EventLogEntryType.Error);
            }

            //Copies a basic WebReports.xml file over to the config directory from backup
            //This is necessary because the REST api will fail to initialize without it
            if (!File.Exists(exagoFolder + "Web\\Config\\WebReports.xml"))
                File.Copy(versionPath + "Backup Files\\WebReports.xml", exagoFolder + "Web\\Config\\WebReports.xml");

            //Applies ExagoREST key to the appSettings file
            string appSettingsFilePath = exagoFolder + "WebServiceApi\\appSettings.config";
            XmlDocument appSettingsXml = new XmlDocument();
            appSettingsXml.Load(appSettingsFilePath);
            XmlNode appSettingsNode = appSettingsXml.SelectSingleNode("//appSettings");
            XmlElement addRestNode = appSettingsXml.CreateElement("add");
            addRestNode.SetAttribute("key", "ExagoREST");
            addRestNode.SetAttribute("value","True");
            appSettingsXml.DocumentElement.AppendChild(addRestNode);
            appSettingsXml.Save(appSettingsFilePath);


            //The .zip file installer doesn't copy the WebServiceApi\Config\WebReportsApi.xml config, so check for that file and copy over a clean backup if it doesn't exist
            string webReportsApiConfigFilePath = exagoFolder + "WebServiceApi\\config\\WebReportsApi.xml";
            if (!File.Exists(webReportsApiConfigFilePath))
                File.Copy(versionPath + "Backup Files\\WebReportsApi.xml", webReportsApiConfigFilePath);
            //Applies the proper AppPath and WebReportsBaseUrl properties to WebReportsApi.xml 
            XmlDocument webReportsApiConfigXml = new XmlDocument();
            webReportsApiConfigXml.Load(webReportsApiConfigFilePath);
            XmlNode appPath = webReportsApiConfigXml.SelectSingleNode("//apppath");
            appPath.InnerText = exagoFolder + "Web\\";
            XmlNode webreportsbaseurl = webReportsApiConfigXml.SelectSingleNode("//webreportsbaseurl");
            webreportsbaseurl.InnerText = "http://localhost/ExagoAstro_" + version + "/";
            webReportsApiConfigXml.Save(webReportsApiConfigFilePath);
        }

        private void setupWebReportsRefrences(string version)
        {
            string exagoFolder = versionPath + version + "\\Exago Folder\\";
            string astroFolder = versionPath + version + "\\Astro Folder\\";
            string WebReportsApiDllFolder = astroFolder + "WebReportsApiDll\\";

            if (!Directory.Exists(WebReportsApiDllFolder))
                Directory.CreateDirectory(WebReportsApiDllFolder);

            foreach(string filePath in Directory.GetFiles(exagoFolder + "WebServiceAPI\\bin"))
            {
                File.Copy(filePath, WebReportsApiDllFolder + Path.GetFileName(filePath), true);
            }
            //File.Copy(exagoFolder + "Web\\bin\\WebReportsApi.dll", WebReportsApiDllFolder + "WebReportsApi.dll", true);       
        }

        //Sets up app pool if it doesn't already exist, then restarts the app pool so that installs can happen
        private void setupAppPool(string version)
        {
            string appPoolName = version + "AstroAppPool";

            using (ServerManager serverManager = new ServerManager())
            {
                bool appPoolExists = false;

                foreach (ApplicationPool appPool in serverManager.ApplicationPools)
                {
                    if (appPool.Name == appPoolName)
                    {
                        appPoolExists = true;
                        break;
                    }
                }

                if (!appPoolExists)
                {
                    ApplicationPool appPool = serverManager.ApplicationPools.Add(appPoolName);
                    appPool.ManagedRuntimeVersion = "v4.0";
                    appPool.Enable32BitAppOnWin64 = false;
                    appPool.ManagedPipelineMode = ManagedPipelineMode.Integrated;
                    serverManager.CommitChanges();
                }

                if(serverManager.ApplicationPools[appPoolName].State != ObjectState.Stopped)
                    serverManager.ApplicationPools[appPoolName].Stop();
            }
        }

        //Sets up the site and then starts the appPool
        private void setupSite(string version)
        {
            string appName = "ExagoAstro_" + version;
            string apiAppName = "ExagoAstroApi_" + version;
            string appPoolName = version + "AstroAppPool";
            string exagoWebFolder = versionPath + version + "\\Exago Folder\\Web\\";
            string exagoApiFolder = versionPath + version + "\\Exago Folder\\WebServiceAPI\\";

            using (ServerManager iisManager = new ServerManager())
            {
                bool appExists = false;
                bool apiAppExists = false;
                ApplicationCollection existingApps = iisManager.Sites["Default Web Site"].Applications;
                foreach(Application app in existingApps)
                {
                    if (app.Path == "/ExagoAstro_" + version)
                        appExists = true;
                    if (app.Path == "/ExagoAstroApi_" + version)
                        apiAppExists = true;
                    if (appExists && apiAppExists)
                        break;
                }
                if (!appExists || !apiAppExists)
                {
                    if (!appExists)
                    {
                        existingApps.Add("/" + appName, exagoWebFolder);
                        //existingApps["/" + appName].VirtualDirectories.Add(appName);
                        existingApps["/" + appName].ApplicationPoolName = appPoolName;
                    }

                    if (!apiAppExists)
                    {
                        existingApps.Add("/" + apiAppName, exagoApiFolder);
                        //existingApps["/" + apiAppName].VirtualDirectories.Add(appName);
                        existingApps["/" + apiAppName].ApplicationPoolName = appPoolName;
                    }
                    iisManager.CommitChanges();
                }

                iisManager.ApplicationPools[appPoolName].Start();
            }
        }

        private void buildAstros(string version)
        {
            string astroFolder = versionPath + version + "\\Astro Folder\\";
            RunProcess("Nuget.exe", "restore " + "\"" + astroFolder + "ASTRO.sln" + "\""); //Requires Nuget > 2.7 to be installed and in the environment path variable
            RunProcess(msbuildPath, "\"" + astroFolder + "ASTRO.sln" + "\"");
        }

        private void RunProcess(string processName, string args)
        {
            writeToEventLog("Running Process: " + processName + " args: " + args);
            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.FileName = processName;
            startInfo.Arguments = args;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;    
            startInfo.RedirectStandardInput = true;
            startInfo.UseShellExecute = false;
            process.StartInfo = startInfo;

            writeToEventLog("Starting Process " + processName);
            process.Start();

            string processStandardOutput = process.StandardOutput.ReadToEnd();
            string processErrorOutput = process.StandardError.ReadToEnd();
            process.WaitForExit();

            if (process.ExitCode == 0)
                writeToEventLog("Process Successful: " + processStandardOutput, EventLogEntryType.SuccessAudit);
            else
            {
                string message = String.Format("Error while running process: {1} {2}{0} Error Message: {3}{0} Standard Output: {5}{0} ExitCode: {4}", Environment.NewLine, processName, args, processErrorOutput, process.ExitCode, processStandardOutput);
                writeToEventLog(message, EventLogEntryType.Error);
            }
        }

        //Helper method called to write to the event log
        public void writeToEventLog(string msg, EventLogEntryType logType = EventLogEntryType.Information, int evtId = -1)
        {
            //EventLog requires messages to be less than or equal to 32766 characters long, so I'll call this on error outputs and truncate to 30000 to avoid any logging issues
            if (msg.Length >= 30000)
                msg =  msg.Substring(0, 30000);

            if (!debugLogging && logType == EventLogEntryType.Information)
                return;
            if (evtId != -1)
                EventLog1.WriteEntry(msg, logType, evtId);
            else
                EventLog1.WriteEntry(msg, logType, eventId++);
            if (Environment.UserInteractive)
                Console.WriteLine(logType == EventLogEntryType.Error ? "Error: " + msg : "Info: " + msg);
        }

        protected override void OnStop()
        {
            writeToEventLog("In OnStop");

            // Update the service state to Stopped pending.
            serviceStatus.dwCurrentState = ServiceState.SERVICE_STOP_PENDING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            // Update the service state to Stopped.
            serviceStatus.dwCurrentState = ServiceState.SERVICE_STOPPED;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        public enum ServiceState
        {
            SERVICE_STOPPED = 0x00000001,
            SERVICE_START_PENDING = 0x00000002,
            SERVICE_STOP_PENDING = 0x00000003,
            SERVICE_RUNNING = 0x00000004,
            SERVICE_CONTINUE_PENDING = 0x00000005,
            SERVICE_PAUSE_PENDING = 0x00000006,
            SERVICE_PAUSED = 0x00000007,
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ServiceStatus
        {
            public int dwServiceType;
            public ServiceState dwCurrentState;
            public int dwControlsAccepted;
            public int dwWin32ExitCode;
            public int dwServiceSpecificExitCode;
            public int dwCheckPoint;
            public int dwWaitHint;
        };

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(System.IntPtr handle, ref ServiceStatus serviceStatus);

    }
}