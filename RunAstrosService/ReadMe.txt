To install, open Developer Command Prompt as admin and run InstallAstro.bat

To open in debug mode, Open up Project -> Properties -> Application and switch Output type to "Console Application" and Startup object to "RunAstrosService.AstroRunService."

To switch back to install as a service, set Output type to "Windows Application" and Startup Object to "RunAstrosService.Program"